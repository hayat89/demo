import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.proxy.CaptureType;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
///Users/bilalhayat/IdeaProjects/mcKinseyAuto/src/test/java/TrafficAnalyzer.java

import java.io.File;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class TrafficAnalyzer
{
    public static void main(String arg[]) throws IOException, InterruptedException {
        BrowserMobProxy proxy = new BrowserMobProxyServer();
        proxy.setTrustAllServers(true);
        proxy.start();

        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);

        String hostIp = Inet4Address.getLocalHost().getHostAddress();

        //seleniumProxy.setHttpProxy(hostIp+":"+ proxy.getPort());
        seleniumProxy.setSslProxy(hostIp+":"+ proxy.getPort());

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.PROXY, seleniumProxy);
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        WebDriver driver = new ChromeDriver(capabilities);

        /*proxy.setHarCaptureTypes(CaptureType.REQUEST_CONTENT,CaptureType.RESPONSE_CONTENT);

        proxy.newHar();*/

        driver.get("https://iservice-v2.trueid-dev.net");


        proxy.setHarCaptureTypes(CaptureType.REQUEST_CONTENT,CaptureType.RESPONSE_CONTENT);

        proxy.newHar();

        Har har = proxy.getHar();

        driver.findElement(By.xpath("//div[@id='header_signup_login_login']")).click();  //click on login

        driver.findElement(By.xpath("//div[@class='input_help']/input[@type='text']")).sendKeys("0866626411");

        driver.findElement(By.xpath("//div[@class='input_help']/input[@type='password']")).sendKeys("11111111");

        driver.findElement(By.xpath("//div[@class='input_help']/button[@id='submit']")).click();

        /*proxy.setHarCaptureTypes(CaptureType.REQUEST_CONTENT,CaptureType.RESPONSE_CONTENT);
          proxy.newHar();
        */
        //Har har = proxy.getHar();
        Thread.sleep(10000);

        File file = new File("/Users/bilalhayat/HAR/trafficAnalyzer1.har");

        har.writeTo(file);

        List<HarEntry> entries = proxy.getHar().getLog().getEntries();
        for(HarEntry entry :entries)
        {
         System.out.println(entry.getRequest().getUrl());
        }

 }
}
